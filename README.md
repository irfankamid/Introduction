## **INTRODUCTION**

### **Biography**

<img src="https://gitlab.com/irfankamid/Introduction/-/raw/main/Gallery%20muka%20saya/ipan_hesmes.jfif" width=150 align=middle>

* **NAME:** MUHAMMAD IRFAN BIN KAMID

* **AGE:** 22

* **MATRIC NO:** 197046

* **COURSE:** Bachelor Of Aerospace Engineering  With Honors

## Strength & Weakness

| Strength| Weakness |
| :----------: | :-----------: |
| extrovert | procrastinate things |
| confident | impulsive | 
| adaptive | lazy |
| spontaneous | undecisive |
